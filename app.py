from datetime import date, datetime

import click
import huepy
from flask import Flask

from core import repository, config, presenters
from core.common import setname, validator, gen_test

app = Flask(__name__)


@app.cli.command()
@click.option('user', '--id', '-i', type=int,
              help='User ID - Should be in range [0, {}]'.format(
                  config.TOTAL_USERS))
def login(user):
    """
    Login for user (identified by id) for today
    """
    if not validator.validate_user_id(user):
        print(huepy.red("Login Failed | Invalid user id"))
        exit(1)

    name = setname.gen_setname(date.today())
    repository.login(user, name)
    print(huepy.green("Login Success | user {}".format(user)))
    exit(0)


@app.cli.command()
def current():
    """Get current reports about logged users"""
    setnames = setname.consecutive_setnames(date.today(), 1)
    reports = repository.consecutive_presence(setnames)
    presenters.print_report("current logged", reports)
    exit(0)


@app.cli.command()
@click.option('days', '--days', '-d', type=int,
              help='Number of days we want to calculate consecutive reports')
def consecutive(days):
    """
    Get reports about all the users who came on consecutive days and those
    who were absent on consecutive days
    """
    if not validator.validate_consecutive_report_days(days):
        print(huepy.red("Invalid consecutive report days"))
        exit(1)

    setnames = setname.consecutive_setnames(date.today(), days)

    # Consecutive Presence
    reports_presence = repository.consecutive_presence(setnames)
    presenters.print_report("consecutive presence", reports_presence)

    # Consecutive Absent
    reports_absent = repository.consecutive_absent(setnames)
    presenters.print_report("consecutive absent", reports_absent)
    exit(0)


@app.cli.command()
@click.option('report_date', '--date', '-d', type=str,
              help='Report date should be in format mm/dd/yyyy')
def report(report_date):
    """
    Report about logged users in one specific date
    """
    if not validator.validate_report_date(report_date):
        print(huepy.red("Invalid report date"))

    setnames = setname.consecutive_setnames(
        datetime.strptime(report_date, "%d/%m/%Y").date(), 1)
    reports = repository.consecutive_presence(setnames)
    presenters.print_report("logged user", reports)
    exit(0)


@app.cli.command()
def test():
    """
    Generate randomly logs for two day, then print consecutive log for
    this two day
    """
    day1_logged_list = gen_test.random_logged_users()
    day1_setname = "test/day/1:login"
    day2_logged_list = gen_test.random_logged_users()
    day2_setname = "test/day/2:login"

    repository.seed_data(day1_setname, day1_logged_list)
    repository.seed_data(day2_setname, day2_logged_list)

    day1_presence = repository.consecutive_presence([day1_setname])
    presenters.print_report("day 1 presence", day1_presence)
    day1_absent = repository.consecutive_absent([day1_setname])
    presenters.print_report("day 1 absent", day1_absent)

    day2_presence = repository.consecutive_presence([day2_setname])
    presenters.print_report("day 2 presence", day2_presence)
    day2_absent = repository.consecutive_absent([day2_setname])
    presenters.print_report("day 2 absent", day2_absent)

    consecutive_presence = repository.consecutive_presence(
        [day1_setname, day2_setname])
    presenters.print_report("consecutive presence", consecutive_presence)
    consecutive_absent = repository.consecutive_absent(
        [day1_setname, day2_setname])
    presenters.print_report("consecutive absent", consecutive_absent)

    exit(0)
