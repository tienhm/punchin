![using python3](https://img.shields.io/badge/using-python3-blue.svg)


# Setup
Your computer must have installed Docker, Python3
### Install requirements
```
$ pip install -r requirements/prod
```
### Run redis
```
$ docker run --name redis-dev -e REDIS_PASSWORD=verysecure -p 6379:6379 -d bitnami/redis
```

# Usage:
### To login for an user
```
$ flask login --id=<user_id>
```
### To get current reports
```
$ flask current
```
### To get report in particular date
{:toc}
```
$ flask report --date="12/06/2018"
```
### To get consecutive report in number of days
```
$ flask consecutive --days=<consecutive_days>
```
### To generate test
```
$ flask test
```
### To get detail help for each command
```
$ flask <command> help
```

# Structure
I follow Clean Structure to build this project skeleton with:
- Configuration Layer: to share project config
- Repository Layer: access database
- Presenters Layer: to present output

They all are used in Actions Layer - which import another layer as
dependency-injected and map with user's action (app.py)

Depends on test requirements, I'm using Redis as database so I don't need to define
ORM - that's reason why I do not have Models Layer here.

# May be you will ask
Q: Why in presenters line 11, we have to add condition that `key < config.TOTAL_USERS`?
A: Because when init a bitstring with length is 100 (for 100 users), it will create
a bitstring with length is 104. Because bitstring can only define with length is 8
multipliers, so it will automatic init at the nearest bigger number which is 8
multiplier.

Q: How much time was I used?
A: I did this tests in four segments:
```
1. From 17:00 11/06/2018 to 18:28 11/06/2018 - Total: 1:28
2. From 12:00 12/06/2018 to 13:00 12/06/2018 - Total: 1:00
3. From 17:00 12/06/2018 to 17:21 12/06/2018 - Total: 0:21
4. From 23:00 12/06/2018 to 00:45 13/06/2018 - Total: 1:58
-----------------------------------------------------------
                                               Total: 4:47
```

Q: How can I track my time?
A: I'm using Watson (https://github.com/TailorDev/Watson)

Q: Am I often manage my time with tool like this?
A: No, this is the first time I try it.

Q: Why did I have to separate this tests in many segments?
A: I have had a meeting in 11/06/2018 morning, a mistake from my customers which
restarted all their Kubernetes cluster (even Production) in 12/06/2018
morning, an English class in 12/06/2018 night, a 5xx message from GitLab in
12/06/2018 night which made me can not get my code. Thank god, it's finally done.

Q: What do I think I can do more in this test if I have more free time?
A: Add lint and unit test