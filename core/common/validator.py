from datetime import datetime

from core import config


def validate_user_id(user_id):
    """
    Validate user id, a valid user id should be integer between 0 and
    config.TOTAL_USERS

    >>> assert validate_user_id(69)
    >>> assert not validate_user_id(100)
    >>> assert not validate_user_id("cat")
    """
    if not isinstance(user_id, int):
        return False
    if not (0 <= user_id < config.TOTAL_USERS):
        return False
    return True


def validate_consecutive_report_days(days):
    """
    Validate consecutive report days - should be a valid positive integer

    >>> assert validate_user_id(1)
    >>> assert not validate_user_id("100")
    >>> assert not validate_user_id(-10)
    """
    return isinstance(days, int) and days > 0


def validate_report_date(report_date):
    """
    Validate report date - should be in format `dd/mm/yyyy`

    >>> assert not validate_report_date(1)
    >>> assert not validate_report_date("cat")
    >>> assert not validate_report_date("12/50/9024")
    >>> assert validate_report_date("20/12/1999")
    """
    try:
        datetime.strptime(report_date, "%d/%m/%Y")
    except (TypeError, ValueError):
        return False
    return True
