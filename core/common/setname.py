from datetime import timedelta


def gen_setname(report_date):
    """
    Login bit string name will be set followed this rule `yyyymmdd:login`
    in Redis. This function return set name based on current date

    >>> from datetime import date
    >>> report_date = date(year=2018, month=6, day=11)
    >>> assert gen_setname(report_date) == "2018/6/11:login"
    """
    return "{year}/{month}/{day}:login".format(
        year=report_date.year, month=report_date.month, day=report_date.day)


def consecutive_setnames(report_date, days):
    """
    Return list of consecutive set name from start date to past

    >>> from datetime import date
    >>> report_date = date(year=2018, month=6, day=11)
    >>> setnames = consecutive_setnames(report_date, 2)
    >>> assert len(setnames) == 2
    >>> assert setnames[0] == "2018/6/11:login"
    >>> assert setnames[1] == "2018/6/10:login"
    """
    setnames = list()
    for i in range(0, days):
        setnames.append(gen_setname(report_date - timedelta(days=i)))
    return setnames
