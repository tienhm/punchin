from random import sample, randint

from core.config import TOTAL_USERS


def random_logged_users():
    """Randomly generate list of logged users"""
    data = sample(
        list(range(TOTAL_USERS)),
        randint(40, 70)
    )
    return sorted(data)
