from uuid import uuid4

from redis import Redis

from core.config import REDIS_HOST, REDIS_PASSWORD, TOTAL_USERS

redis = Redis(host=REDIS_HOST, password=REDIS_PASSWORD)


def consecutive_presence(setnames):
    """Get consecutive presence list of users"""
    init_sets(setnames)

    result_set = str(uuid4())
    redis.bitop("AND", result_set, *setnames)

    rs = redis.get(result_set)
    redis.delete(result_set)
    return list(
        map(
            int,
            ''.join([bin(char).lstrip('0b').rjust(8, '0') for char in rs])
        )
    )


def consecutive_absent(setnames):
    """Get consecutive absent list of users"""
    init_sets(setnames)
    result_set = str(uuid4())

    tmp_set = result_set + "_tmp"
    redis.bitop("OR", tmp_set, *setnames)
    redis.bitop("NOT", result_set, tmp_set)
    rs = redis.get(result_set)

    redis.delete(tmp_set)
    redis.delete(result_set)
    return list(
        map(
            int,
            ''.join([bin(char).lstrip('0b').rjust(8, '0') for char in rs])
        )
    )


def login(user_id, setname):
    """Login for an user in report date"""
    init_set(setname)
    return redis.setbit(setname, user_id, 1)


def init_set(setname):
    existed = redis.exists(setname)
    if not existed:
        redis.setbit(setname, TOTAL_USERS, 0)


def init_sets(setnames):
    for setname in setnames:
        init_set(setname)
    return


def seed_data(setname, logged_list):
    """
    Seed data for a set with list of user id who is logged in
    :param setname: name of set - based on date
    :param logged_list: list of user id who is logged in
    """
    redis.delete(setname)
    init_set(setname)
    for user_id in logged_list:
        redis.setbit(setname, user_id, 1)
