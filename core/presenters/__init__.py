import huepy

from core import config


def print_report(report_type, reports):
    """Print basic report"""
    report_type = report_type.title()

    ids = [key for key, value in enumerate(reports)
           if value == 1 and key < config.TOTAL_USERS]
    total = len(ids)

    print(huepy.green(report_type))
    print("Total:".format(report_type), total)
    print("List user ids:")
    print(ids)
    print("=" * 50)
